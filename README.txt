Zoho Campaigns Plugin for Drupal
                        
'Drupal' is an open source content management platform empowering huge number of websites and applications. Using, Drupal, you can build websites and write blogs, and make use of web forms. 
Zoho Campaigns plugin for Drupal allows the subscribers to get added in your mailing list present in your Zoho Campaigns account. All you need is to create a mailing list based sign-up form which can be added in your website. This plugin helps you to manage mailing list, subscribers, and track performance.
 
Prerequisites
Web Server: Apache or Microsoft IIS
PHP: 5.2 or higher
Database Server: MySQL - 5.0 or higher, PostgreSQL - 8.3 or higher, or SQLite
 
Installation of Drupal on your machine
Before you integrate your Zoho Campaigns with Drupal, you need to download and install the updated Drupal software on your machine.
 
To install Drupal, follow the instructions below:
 
Go to Drupal website and download the latest version.
Now, go to Modules tab in the top panel to install the new module 
Choose from files and select the downloaded Drupal 7.
Now, click Install and enable the 'ZohoCampaigns' module.
Save the configuration.
  
Integration with Zoho Campaigns
                
To integrate with Drupal, follow the instructions below:
 
Select configuration from the top panel and start configuring Zoho Campaigns from the list of web services available.
 Go to Zoho Campaigns plugin -> API key
 Give your zoho campaigns email id
Paste your API key code generated from the API and Call Back tab of your campaigns account.
'Batch Limit' drop down list - Select the number of contacts to be moved to the campaigns list at a single time based on traffic. You can select contacts ranging from 10 to 10,000 contacts. Finally, save the configuration and proceed with the sign-up form creation tab.
 
Create Sign-up form
                     In this step, the blocks created for every lists are shown. In Drupal, the boxes which can be made to appear in various regions of a Drupal website are called Blocks.
 
Sign-up form Settings                   
Header
Fields
Forms
Buttons
 
Using headers
                    
Use the editable header text available in the header and give a catchy title to your sign-up form. You can change the title text color using Header text color.
 
You can change the header font color, size, and alignment for the best appearance of your sign-up form title.
NOTE: The changes are saved automatically when you switch over between the tabs.
 
Fields Settings
                      You can add up to a maximum of eight fields and it also increases the sign-up form height dynamically whenever you tick the fields you want to add.
 
Display settings
                      Label position has three options namely : Inline, placeholder, and top
Inline      : The label will be present exactly to the left of the fields.
Placeholder  : The label will be present inside the fields
Top :  The label will be present at the top of the fields
                   
              
Note        :  1.  Use the text box color and field label color to show the richness of the form.
                  2.  Turn off the description of the labels or you can switch over between "on
                     mouse hover" and "gray".
 
Important : Description size is accessible only for "gray" option.
 
Form :
                    You can enrich the look and feel of your form by changing its background color and width(200-850 limit).
 
Buttons :
Do change the position, text, text color and background color of the button.
Also, choose the shape you want the button to get displayed.
 
 
IMPORT FORM(3rd step) :
                           You can give a name for the block you are going to import from the lists of campaigns that has a sign-up form available in the mailing list drop down box. The sign-up form will be imported from the campaigns.
 
Drupal Glossary :
                 API: An application programming interface (API) is a particular
                         set of rules (�code�) and specifications that software program can follow to      
                         communicate with each other.
            CRON : The 'cron' functions more like a command scheduler.It schedules the work                      
                         into multiple tasks and runs specific data at successive intervals of time.
BATCH LIMIT : In batch limit, you decide the quantity of the data to be pushed in to the
                         queue for processing in a single session. It avoids the over traffic of data and
                         streamlines the flow of data
    LIBRARY   : Library of code from a third party. It is a module that will help you manage
                        and load these.
Source : https://www.drupal.org/
 
 
 
 



