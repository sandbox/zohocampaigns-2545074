/**
 * @file
 */
(function($) {
  Drupal.behaviors.change1 = {
    attach: function() {

	    $('label').each(function() {
			              if( $(this).attr('for').match(/edit-email/)) {
                                              $(this).css("float","left");
					      $(this).css("font-size","15px");
					      $(this).css("margin-left","30%");
                                              $(this).css("padding-top","11px");
                                              $(this).css("font-weight","normal");					      
					      

				      }
                                      if( $(this).attr('for').match(/edit-key/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");
					      $(this).css("margin-left","30%");
                                              $(this).css("padding-top","11px");
                                              $(this).css("font-weight","normal");					      
					      
					      
			         }
                                      if( $(this).attr('for').match(/edit-batch-limit/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");
					      $(this).css("margin-left","30%");
                                              $(this).css("padding-top","11px");					      
                                              $(this).css("font-weight","normal");					      
					      
			         }
				      if( $(this).attr('for').match(/edit-name/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");	
                                              $(this).css("padding-left","26%");
                                              $(this).css("padding-top","11px");					      
                                              $(this).css("font-weight","normal");					      
					      
					      
				      }
				      if( $(this).attr('for').match(/edit-id/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");
                                              $(this).css("padding-left","26%");					      
                                              $(this).css("padding-top","11px");					      
                                              $(this).css("font-weight","normal");					      
					      
				      }
				       if( $(this).attr('for').match(/edit-btext/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");
                                              $(this).css("padding-left","26%");					      
                                              $(this).css("padding-top","11px");					      
                                              $(this).css("font-weight","normal");					      
					      
				      }
				       if( $(this).attr('for').match(/edit-listid/)) {
                                              $(this).css("float","left");
                                              $(this).css("font-size","15px");
                                              $(this).css("padding-left","26%");					      
                                              $(this).css("padding-top","11px");					      
                                              $(this).css("font-weight","normal");					      
					      
				      }
                                });
	    
	    $((document.getElementById("edit-email"))).css("margin-left","-24%");
	    $((document.getElementById("edit-email"))).css("width","30%");	 
	    $((document.getElementById("edit-email"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-email"))).css("border-radius","3px");
	    $((document.getElementById("edit-email"))).css("height","37px");
	    $((document.getElementById("edit-email"))).css("text-indent","15px");
	    $((document.getElementById("edit-email"))).css("font-size","15px");
	    $((document.getElementById("edit-email"))).css("position","relative");  
            $((document.getElementById("edit-name"))).css("margin-left","-24%");
	    $((document.getElementById("edit-name"))).css("width","30%");
    	    $((document.getElementById("edit-name"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-name"))).css("border-radius","3px");
	    $((document.getElementById("edit-name"))).css("height","37px");
	    $((document.getElementById("edit-name"))).css("text-indent","15px");
	    $((document.getElementById("edit-name"))).css("font-size","15px");
	    $((document.getElementById("edit-name"))).css("position","relative");

	    $((document.getElementById("edit-btext"))).css("margin-left","5%");
	    $((document.getElementById("edit-btext"))).css("width","30%");
    	    $((document.getElementById("edit-btext"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-btext"))).css("border-radius","3px");
	    $((document.getElementById("edit-btext"))).css("height","37px");
	    $((document.getElementById("edit-btext"))).css("text-indent","15px");
	    $((document.getElementById("edit-btext"))).css("font-size","15px");
	    $((document.getElementById("edit-btext"))).css("position","relative");
	    $((document.getElementById("edit-listid"))).css("margin-left","5%");
	    $((document.getElementById("edit-listid"))).css("width","30%");	    
            $((document.getElementById("edit-listid"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-listid"))).css("border-radius","3px");
	    $((document.getElementById("edit-listid"))).css("height","37px");
	    $((document.getElementById("edit-listid"))).css("text-indent","15px");
	    $((document.getElementById("edit-listid"))).css("font-size","15px");
	    $((document.getElementById("edit-listid"))).css("position","relative");
 	    
            $((document.getElementById("edit-id"))).css("margin-left","-24%");
	    $((document.getElementById("edit-id"))).css("width","30%");	    
            $((document.getElementById("edit-id"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-id"))).css("border-radius","3px");
	    $((document.getElementById("edit-id"))).css("height","37px");
	    $((document.getElementById("edit-id"))).css("text-indent","15px");
	    $((document.getElementById("edit-id"))).css("font-size","15px");
	    $((document.getElementById("edit-id"))).css("position","relative");
	    
	    $((document.getElementById("edit-key"))).css("margin-left","-20%");
	    $((document.getElementById("edit-key"))).css("width","30%");
	    $((document.getElementById("edit-key"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-key"))).css("border-radius","3px");
	    $((document.getElementById("edit-key"))).css("height","37px");
	    $((document.getElementById("edit-key"))).css("text-indent","15px");
	    $((document.getElementById("edit-key"))).css("font-size","15px");
	    $((document.getElementById("edit-key"))).css("position","relative");
	    $((document.getElementById("edit-batch-limit"))).css("margin-left","-22%");
	    $((document.getElementById("edit-batch-limit"))).css("width","30%");
	    $((document.getElementById("edit-batch-limit"))).css("border","1px solid #d8d8d8");
	    $((document.getElementById("edit-batch-limit"))).css("border-radius","3px");
	    $((document.getElementById("edit-batch-limit"))).css("height","37px");
	    $((document.getElementById("edit-batch-limit"))).css("text-indent","15px");
	    $((document.getElementById("edit-batch-limit"))).css("font-size","15px");
	    $((document.getElementById("edit-batch-limit"))).css("position","relative");
	    $((document.getElementsByName("op_save"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("op_save"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("op_save"))[0]).css("color", "white");
	    $((document.getElementsByName("op_save"))[0]).css("padding", "10px");
	    $((document.getElementsByName("op_save"))[0]).css("width", "193px");
            
	    $((document.getElementsByName("backno"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("backno"))[0]).css("background","gray");
	    $((document.getElementsByName("backno"))[0]).css("color", "white");
	    $((document.getElementsByName("backno"))[0]).css("padding", "10px");
	    $((document.getElementsByName("backno"))[0]).css("width", "193px");
	    $((document.getElementsByName("backno"))[0]).css("margin-left", "42%"); 
	    
	    $((document.getElementsByName("back"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("back"))[0]).css("background","gray");
	    $((document.getElementsByName("back"))[0]).css("color", "white");
	    $((document.getElementsByName("back"))[0]).css("padding", "10px");
	    $((document.getElementsByName("back"))[0]).css("width", "193px");
	    $((document.getElementsByName("back"))[0]).css("margin-left", "4%"); 
	    

	    $((document.getElementsByName("create"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("create"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("create"))[0]).css("color", "white");
	    $((document.getElementsByName("create"))[0]).css("padding", "10px");
	    $((document.getElementsByName("create"))[0]).css("width", "193px");
	    $((document.getElementsByName("create"))[0]).css("margin-left", "55%"); 
	   
	   $((document.getElementsByName("import"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("import"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("import"))[0]).css("color", "white");
	    $((document.getElementsByName("import"))[0]).css("padding", "10px");
	    $((document.getElementsByName("import"))[0]).css("width", "193px");
	    $((document.getElementsByName("import"))[0]).css("margin-left", "82%"); 

	    $(document.getElementById('edit-actions-create')).css("padding-top","16px");
	    $(document.getElementById('edit-actions-create')).css("background-color","#e7e7e7");
	    $(document.getElementById('edit-actions-create')).css("margin-left","-33px");
	    $(document.getElementById('edit-actions-create')).css("margin-right","-33px");
	    $(document.getElementById('edit-actions-create')).css("margin-bottom","-7px");

	    $(document.getElementById('edit-actions-save')).css("margin-top","-10px");
	    $(document.getElementById('edit-actions-save')).css("padding-top","16px");
	    $(document.getElementById('edit-actions-save')).css("background-color","#e7e7e7");
	    $(document.getElementById('edit-actions-save')).css("margin-left","-33px");
	    $(document.getElementById('edit-actions-save')).css("margin-right","-33px");
	    $(document.getElementById('edit-actions-save')).css("margin-bottom","-7px");
	    
	    $(document.getElementById('edit-actions-import')).css("padding-top","16px");
	    $(document.getElementById('edit-actions-import')).css("background-color","#e7e7e7");
	    $(document.getElementById('edit-actions-import')).css("margin-left","-33px");
	    $(document.getElementById('edit-actions-import')).css("margin-right","-33px");
	    $(document.getElementById('edit-actions-import')).css("margin-bottom","-7px");

	    $(document.getElementById('edit-actions-import1')).css("padding-top","16px");
	    $(document.getElementById('edit-actions-import1')).css("background-color","#e7e7e7");
	    $(document.getElementById('edit-actions-import1')).css("margin-left","-33px");
	    $(document.getElementById('edit-actions-import1')).css("margin-right","-33px");
	    $(document.getElementById('edit-actions-import1')).css("margin-bottom","-7px");

	    $((document.getElementsByName("import1"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("import1"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("import1"))[0]).css("color", "white");
	    $((document.getElementsByName("import1"))[0]).css("padding", "10px");
	    $((document.getElementsByName("import1"))[0]).css("width", "150px"); 
	    $((document.getElementsByName("import1"))[0]).css("margin-left", "66%"); 
            $((document.getElementsByName("back1"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("back1"))[0]).css("background","gray");
	    $((document.getElementsByName("back1"))[0]).css("color", "white");
	    $((document.getElementsByName("back1"))[0]).css("padding", "10px");
	    $((document.getElementsByName("back1"))[0]).css("width", "150px"); 
	    $((document.getElementsByName("back1"))[0]).css("margin-left", "3%"); 

	    $((document.getElementsByName("save"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("save"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("save"))[0]).css("color", "white");
	    $((document.getElementsByName("save"))[0]).css("padding", "10px");
	    $((document.getElementsByName("save"))[0]).css("width", "150px"); 
	    $((document.getElementsByName("save"))[0]).css("margin-left", "66%"); 
            $((document.getElementsByName("revert"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("revert"))[0]).css("background","gray");
	    $((document.getElementsByName("revert"))[0]).css("color", "white");
	    $((document.getElementsByName("revert"))[0]).css("padding", "10px");
	    $((document.getElementsByName("revert"))[0]).css("width", "150px"); 
	    $((document.getElementsByName("revert"))[0]).css("margin-left", "3%"); 
	    
	    $((document.getElementById("edit-label"))).css("padding-top","3px");	    
	    $((document.getElementById("edit-label"))).css("border-radius","4px");
	    $((document.getElementById("edit-label"))).css("width","150px");	    	    
            $((document.getElementById("edit-blabel"))).css("padding-top","3px");	    
	    $((document.getElementById("edit-blabel"))).css("border-radius","4px");
	    $((document.getElementById("edit-blabel"))).css("width","100px");		    	    
	    $((document.getElementsByClassName("fieldset-wrapper"))[1]).css("padding-left","45px");
            $((document.getElementsByClassName("fieldset-wrapper"))[3]).css("padding-left","45px");
            $((document.getElementsByClassName("fieldset-wrapper"))[5]).css("padding-left","45px");
            $((document.getElementsByClassName("fieldset-wrapper"))[7]).css("padding-left","45px");
	    $((document.getElementById("edit-create"))).css("border-radius","4px");
	    $((document.getElementById("edit-create"))).css("background","#58b75f");
	    $((document.getElementById("edit-create"))).css("color", "white");
	    $((document.getElementById("edit-create"))).css("padding", "10px");
	    $((document.getElementById("edit-create"))).css("width", "19%"); 
	    $((document.getElementById("edit-create"))).css("margin-left", "81%");
	    
	     $((document.getElementById("edit-create-empty"))).css("border-radius","4px");
	    $((document.getElementById("edit-create-empty"))).css("background","#58b75f");
	    $((document.getElementById("edit-create-empty"))).css("color", "white");
	    $((document.getElementById("edit-create-empty"))).css("padding", "10px"); 
	    $(document.getElementById('edit-actions-delete')).css("text-align","center");
	    $(document.getElementById('edit-actions-delete')).css("padding-top","16px");
	    $(document.getElementById('edit-actions-delete')).css("background-color","#e7e7e7");
	    $(document.getElementById('edit-actions-delete')).css("margin-left","-33px");
	    $(document.getElementById('edit-actions-delete')).css("margin-right","-33px");
	    $(document.getElementById('edit-actions-delete')).css("margin-bottom","-7px");
	    $((document.getElementsByName("cancel"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("cancel"))[0]).css("background","Gray");
	    $((document.getElementsByName("cancel"))[0]).css("color", "white");
	    $((document.getElementsByName("cancel"))[0]).css("padding", "10px");
	    $((document.getElementsByName("cancel"))[0]).css("width", "150px"); 
	    $((document.getElementsByName("delete"))[0]).css("border-radius","4px");
	    $((document.getElementsByName("delete"))[0]).css("background","#58b75f");
	    $((document.getElementsByName("delete"))[0]).css("color", "white");
	    $((document.getElementsByName("delete"))[0]).css("padding", "10px");
	    $((document.getElementsByName("delete"))[0]).css("width", "150px"); 
	 var a =(Drupal.settings['zohocampaigns']['n1']);
	    for(var i=0;i<a;i++)
{
	    $(document.getElementsByName("SIGNUP_BODY")[i]).css("display","");
	    $(document.getElementsByName("SIGNUP_PAGE")[i]).css("display","");	  
	    $(document.getElementsByName("SIGNUP_PAGE")[i]).css("position","");	    	    

}
         $(document.getElementById("edit-style")).css("cursor","pointer");
         $(document.getElementById("edit-size")).css("cursor","pointer"); 
         $(document.getElementById("edit-hbold1")).css("cursor","pointer"); 
         $(document.getElementById("edit-hitalic1")).css("cursor","pointer"); 
         $(document.getElementById("edit-hunder1")).css("cursor","pointer"); 
         $(document.getElementById("edit-left")).css("cursor","pointer"); 
         $(document.getElementById("edit-center")).css("cursor","pointer"); 
         $(document.getElementById("edit-right")).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[0]).css("cursor","pointer"); 
         $(document.getElementById("edit-style1")).css("cursor","pointer"); 
         $(document.getElementById("edit-size1")).css("cursor","pointer"); 
         $(document.getElementById("edit-bold1")).css("cursor","pointer"); 
         $(document.getElementById("edit-italic1")).css("cursor","pointer"); 
         $(document.getElementById("edit-under1")).css("cursor","pointer"); 
         $(document.getElementById("edit-left1")).css("cursor","pointer"); 
         $(document.getElementById("edit-center1")).css("cursor","pointer"); 
         $(document.getElementById("edit-right1")).css("cursor","pointer"); 
         $(document.getElementById("other")).css("cursor","pointer"); 
         $(document.getElementById("edit-style")).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[1]).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[2]).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[3]).css("cursor","pointer");
	 $((document.getElementsByClassName("ui-slider-handle"))[0]).css("cursor","pointer"); 
         $(document.getElementById("edit-bstyle")).css("cursor","pointer"); 
         $(document.getElementById("edit-bsize")).css("cursor","pointer"); 
         $(document.getElementById("edit-bleft")).css("cursor","pointer"); 
         $(document.getElementById("edit-bcenter")).css("cursor","pointer"); 
         $(document.getElementById("edit-bright")).css("cursor","pointer"); 
         $(document.getElementById("edit-bshape")).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[4]).css("cursor","pointer"); 
         $((document.getElementsByClassName("color_picker"))[5]).css("cursor","pointer"); 
         $(document.getElementById("edit-addmore")).css("cursor","pointer");
         $(document.getElementById("edit-opt")).css("cursor","pointer"); 
         $(document.getElementById("edit-opt1")).css("cursor","pointer"); 
         //$(document.getElementById("edit-opt1")).css("margin-left","11%"); 
	  
	 $('#total').niceScroll();
	 $('.form1').niceScroll();
	 $('.form2').niceScroll();	 
	 $('.form3').niceScroll();	 
	 $('.form5').niceScroll();	 
	 $('.form6').niceScroll();	 
	 $('.form7').niceScroll();	 
	    $('.vertical-tab-button').find("a").once().click(function() {
		     $("#edit-label").blur();
	    });
           

            $(document.getElementById("edit-back")).once().click(function() {
		     var back = confirm("Moving back will delete the form.\nAre you sure you want to go back?"); 
		     if(back == true) {
			      ((document.getElementsByName("yes"))[0]).value = 1;
                              ((document.getElementsByName("no"))[0]).value = 0;
		     }
		     else {
			      ((document.getElementsByName("yes"))[0]).value = 0;
                              ((document.getElementsByName("no"))[0]).value = 1;
		     }
	    });
           
	$('label').each(function() {

		if( $(this).attr('for').match(/edit-opt/)) {
			$(this).css("float","left");
			$(this).css("margin","5px");
			$(this).css("margin-right","11%");						
		}
                if( $(this).attr('for').match(/edit-opt1/)) {
			$(this).css("float","left");
			$(this).css("margin","5px");
			$(this).css("margin-right","5%");						
			
		}
                if( $(this).attr('for').match(/edit-des-size/)) {
			$(this).css("float","left");
			$(this).css("margin","5px");
			$(this).css("margin-right","6%");			
			
		}
	}); 
         $(document).click(function(e) {

         if( e.target.id != 'other') {
            if( e.target.id != 'edit-opt') {
	   if( e.target.id != 'edit-opt1') {
		   if(e.target.id != 'edit-des-size')
	 {
         $("#settings").hide();
         $(document.getElementById("other")).css("background-color","#0f273a");	       	 
	 
	}

	   }
	    }
          }
         });
 	    $(document.getElementById("add")).css("margin-left","43px");
            $(document.getElementById("settings")).hide();
  	    $(document.getElementById("other")).once().click(function() {
       	         var x = ((document.getElementsByName("othersettings"))[0]).value;
                 if(x==0)
	    {		   
                 $(document.getElementById("settings")).show();	  
                 ((document.getElementsByName("othersettings"))[0]).value = 1;
	    }
		 else
	    {
                 $(document.getElementById("settings")).hide();	  
                 ((document.getElementsByName("othersettings"))[0]).value = 0;
            }
	    });
    	            $((document.getElementById("edit-id"))).change(function() {
                         $("#edit-id option:contains('- Select -')").remove();
		    });	    
	             var a =  (document.getElementById("edit-des-size")).value+"px";
		    $(document.getElementsByClassName("description")).css("font-size",a);	
		    
	            $((document.getElementById("edit-des-size"))).change(function() {
		       var a =  (document.getElementById("edit-des-size")).value+"px";
		    $(document.getElementsByClassName("description")).css("font-size",a);	
		    });

		   var option =  (document.getElementById("edit-opt1")).value;
		       if(option == "Off")
		    {
			    $('#edit-description0').attr('disabled',true);
			    $('#edit-description1').attr('disabled',true);
			    $('#edit-description2').attr('disabled',true);
			    $('#edit-des-size').attr('disabled','true');
			    $('#edit-des-size').css("background-color","gainsboro");
			    $('#edit-des-size').css("cursor","default");			    				    
			    
	            }
		       else if(option == "OnmouseOver")
		    {
                            $('#edit-description0').attr('disabled',false);
			    $('#edit-description1').attr('disabled',false);
			    $('#edit-description2').attr('disabled',false);
			    $('#edit-des-size').attr('disabled',true);
			    $('#edit-des-size').css("background-color","gainsboro");
			    $('#edit-des-size').css("cursor","default");			    				    			    
			    
			    
	            } 
		       else
		       {
                            $('#edit-description0').attr('disabled',false);
			    $('#edit-description1').attr('disabled',false);
			    $('#edit-description2').attr('disabled',false);
			    $('#edit-des-size').attr('disabled',false);
			    $('#edit-des-size').css("background-color","white");
			    $('#edit-des-size').css("cursor","pointer");			    
			    
			}
	            $((document.getElementById("edit-opt1"))).change(function() {
		       var option =  (document.getElementById("edit-opt1")).value;
		       if(option == "Off")
		    {
			    $('#edit-description0').attr('disabled',true);
			    $('#edit-description1').attr('disabled',true);
			    $('#edit-description2').attr('disabled',true);
			    $('#edit-des-size').attr('disabled',true);			    
			    
			    
	            }
		       else if(option == "OnmouseOver")
		    {
                            $('#edit-description0').attr('disabled',false);
			    $('#edit-description1').attr('disabled',false);
			    $('#edit-description2').attr('disabled',false);
			    $('#edit-des-size').attr('disabled',true);
			    
			    
	            }
		     else
		       {
                            $('#edit-description0').attr('disabled',false);
			    $('#edit-description1').attr('disabled',false);
			    $('#edit-description2').attr('disabled',false);
			    $('#edit-des-size').attr('disabled',false);
			    
			}
		    });	    
	        
            $(document.getElementsByClassName("form-item-preview1-Email")[0]).css("padding","0px");
            $(document.getElementsByClassName("form-item-preview1-Name")[0]).css("padding","0px");
            $(document.getElementsByClassName("form-item-preview1-Phone")[0]).css("padding","0px");
	    $(document.getElementsByClassName("form-type-textfield")[10]).css("padding","0px");	    
	    $(document.getElementsByClassName("form-type-textfield")[11]).css("padding","0px");
	    $(document.getElementsByClassName("form-type-textfield")[12]).css("padding","0px");	    
	    $(document.getElementsByName("op1")[0]).attr('disabled','disabled');
	    $(document.getElementsByName("op1")[1]).attr('disabled','disabled');
	    $(document.getElementsByName("op1")[2]).attr('disabled','disabled');	    
	    $(document.getElementsByName("op1")[3]).attr('disabled','disabled');	    	    
	    var a = (document.getElementById("edit-bstyle")).value;
	    $(document.getElementsByName("op1")[0]).css("font-family",a,'important');
	    $(document.getElementsByName("op1")[1]).css("font-family",a,'important');
	    $(document.getElementsByName("op1")[2]).css("font-family",a,'important');
	    $(document.getElementsByName("op1")[3]).css("font-family",a,'important');      
 var val = $('#edit-slider-container').slider("option","value");
 $((document.getElementsByClassName("fieldset-wrapper"))[1]).width(val);
 $((document.getElementsByClassName("fieldset-wrapper"))[3]).width(val);
 $((document.getElementsByClassName("fieldset-wrapper"))[5]).width(val);
 $((document.getElementsByClassName("fieldset-wrapper"))[7]).width(val);

       			
       var m;
       f = $((document.getElementById("Email"))).css("float");

	       if(f=="left")
	      m = 150;
      else
             m=50;
     $((document.getElementsByName("preview1[Email]"))[0]).width(val-m);
     $((document.getElementsByName("preview1[Name]"))[0]).width(val-m); 
     $((document.getElementsByClassName("formbox"))).width(val-m);
		 
		     var x = ((document.getElementsByName("border_radius"))[0]).value;
	             if(x == "0px")
{
                         $((document.getElementsByName("op1"))[0]).css("border-radius","0px");
                         $((document.getElementsByName("op1"))[1]).css("border-radius","0px");
                         $((document.getElementsByName("op1"))[2]).css("border-radius","0px");	
                         $((document.getElementsByName("op1"))[3]).css("border-radius","0px");				 
		         document.getElementById("button_style").innerHTML = "Sharp Edge";
			 $(document.getElementById("button_style")).css("border-radius","0px")	 
		
}
else if(x == "5px")
{
	                  $((document.getElementsByName("op1"))[0]).css("border-radius","5px");
	                  $((document.getElementsByName("op1"))[1]).css("border-radius","5px");
	                  $((document.getElementsByName("op1"))[2]).css("border-radius","5px");	
	                  $((document.getElementsByName("op1"))[3]).css("border-radius","5px");				  
		          document.getElementById("button_style").innerHTML = "Blunt Edge";	
  		         $(document.getElementById("button_style")).css("border-radius","5px");			  
			 
}
else if(x == "20px")
{
	                  $((document.getElementsByName("op1"))[0]).css("border-radius","20px");
	                  $((document.getElementsByName("op1"))[1]).css("border-radius","20px");
	                  $((document.getElementsByName("op1"))[2]).css("border-radius","20px");
	                  $((document.getElementsByName("op1"))[3]).css("border-radius","20px");			  			  
		         document.getElementById("button_style").innerHTML = "Ellipse";				 
			((document.getElementsByName("border_radius"))[0]).value="20px";
		         $(document.getElementById("button_style")).css("border-radius","20px")
}
	            $((document.getElementById("edit-bshape"))).change(function() {
			    var shape = ((document.getElementById("edit-bshape"))).value;
			 if(shape == 'Ellipse')
		    {
                         $((document.getElementsByName("op1"))[0]).css("border-radius","20px");
                         $((document.getElementsByName("op1"))[1]).css("border-radius","20px");
                         $((document.getElementsByName("op1"))[2]).css("border-radius","20px");
                         $((document.getElementsByName("op1"))[3]).css("border-radius","20px");			 
		         ((document.getElementsByName("border_radius"))[0]).value="20px";			 
		         document.getElementById("button_style").innerHTML = "Ellipse";
		         $(document.getElementById("button_style")).css("border-radius","20px");	 
		    }
			 else if(shape == 'Sharp Edge')
		    {
                         $((document.getElementsByName("op1"))[0]).css("border-radius","0px");
                         $((document.getElementsByName("op1"))[1]).css("border-radius","0px");
                         $((document.getElementsByName("op1"))[2]).css("border-radius","0px");
                         $((document.getElementsByName("op1"))[3]).css("border-radius","0px");			 
			((document.getElementsByName("border_radius"))[0]).value="0px";
		         document.getElementById("button_style").innerHTML = "Sharp Edge";	
 		         $(document.getElementById("button_style")).css("border-radius","0px")
			
		    }
			 else if(shape == 'Rounded Edge')
		    {
                        $((document.getElementsByName("op1"))[0]).css("border-radius","5px");
                         $((document.getElementsByName("op1"))[1]).css("border-radius","5px");
                         $((document.getElementsByName("op1"))[2]).css("border-radius","5px");
                         $((document.getElementsByName("op1"))[3]).css("border-radius","5px");			
			((document.getElementsByName("border_radius"))[0]).value="5px";	
		         document.getElementById("button_style").innerHTML = "Blunt Edge";	
			 $(document.getElementById("button_style")).css("border-radius","5px")
		    }
		    });

		    
		     var x = ((document.getElementsByName("more"))[0]).value;
		     if(x == 0)
                   {
			$(document.getElementById("drag2")).hide();
			$(document.getElementById("drag3")).hide();
			$(document.getElementById("drag4")).hide();
			$(document.getElementById("drag5")).hide();
			$(document.getElementById("drag6")).hide();
			$(document.getElementById("drag7")).hide();
			
                        $(document.getElementById("edit-addmore")).css("background",'rgb(88, 183, 95)');
			$(document.getElementById("edit-addmore")).css("border-radius","4px");
			$(document.getElementById("edit-addmore")).css("color","white");

                   }
	    
                     	$(document.getElementById("edit-addmore")).once().click(function() {
		        var x = ((document.getElementsByName("more"))[0]).value;
		        if(x == 0)
			{
			((document.getElementsByName("more"))[0]).value="1";
			$(document.getElementById("drag2")).show();
			$(document.getElementById("drag3")).show();
			$(document.getElementById("drag4")).show();
			$(document.getElementById("drag5")).show();
			$(document.getElementById("drag6")).show();
			$(document.getElementById("drag7")).show();
                        
			 $(document.getElementById("edit-addmore")).css("background",'gray');
			 $(document.getElementById("edit-addmore")).css("border-color",'gray');			 
			$(document.getElementById("edit-addmore")).css("border-radius","4px");
			$(document.getElementById("edit-addmore")).css("color","white");
			$(document.getElementById("edit-addmore")).css("cursor","default");
			
			    $('#edit-addmore').attr('disabled',true);
			
			}			
			});
       
        $(document.getElementById("edit-label")).keyup( function() {
		         
	                   ((document.getElementsByClassName("head"))[0]).innerHTML = (document.getElementById("edit-label")).value;
	                   ((document.getElementsByClassName("head"))[1]).innerHTML = (document.getElementById("edit-label")).value;		
	                   ((document.getElementsByClassName("head"))[2]).innerHTML = (document.getElementById("edit-label")).value;
	                   ((document.getElementsByClassName("head"))[3]).innerHTML = (document.getElementById("edit-label")).value;		
	});
         $(document.getElementById("edit-label")).change( function() {
		 var label = (document.getElementById("edit-label")).value;
			   var length = label.length;
			   if(length>0)
	{
	                   ((document.getElementsByClassName("head"))[0]).innerHTML = (document.getElementById("edit-label")).value;
	                   ((document.getElementsByClassName("head"))[1]).innerHTML = (document.getElementById("edit-label")).value;		
	                   ((document.getElementsByClassName("head"))[2]).innerHTML = (document.getElementById("edit-label")).value;
	                   ((document.getElementsByClassName("head"))[3]).innerHTML = (document.getElementById("edit-label")).value;			   	   		}
			   else
	{
                           (document.getElementById("edit-label")).value='Join Our Newsletter';
	                   ((document.getElementsByClassName("head"))[0]).innerHTML = 'Join Our Newsletter';	
	                   ((document.getElementsByClassName("head"))[1]).innerHTML = 'Join Our Newsletter';	
	                   ((document.getElementsByClassName("head"))[2]).innerHTML = 'Join Our Newsletter';	
	                   ((document.getElementsByClassName("head"))[3]).innerHTML = 'Join Our Newsletter';	
			   

	}	
			   
	});
        $(document.getElementById("edit-blabel")).keyup( function() {
	                   ((document.getElementById("edit-preview1-submit--3"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit--2"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit--4"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit"))).value = (document.getElementById("edit-blabel")).value;
			   ((document.getElementsByName("op1"))).value = (document.getElementById("edit-blabel")).value;
			   
	});
        $(document.getElementById("edit-blabel")).change( function() {
	                   ((document.getElementById("edit-preview1-submit--3"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit--2"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit--4"))).value = (document.getElementById("edit-blabel")).value;
	                   ((document.getElementById("edit-preview1-submit"))).value = (document.getElementById("edit-blabel")).value;
			   
	});
        $(document.getElementById("edit-size")).change( function() {
		           var size =(document.getElementById("edit-size")).value;
                           (document.getElementById("font_size")).innerHTML=size;
                           size+="px";			   
                          $((document.getElementsByClassName("head"))).css("font-size",size,'important');          
	});
        $(document.getElementById("edit-style")).change( function() {
		           var style =(document.getElementById("edit-style")).value;
                           (document.getElementById("font_family")).innerHTML=style;
			  $((document.getElementsByClassName("head"))).css("font-family",style,'important');          
		      	  
	});
	$((document.getElementsByClassName("collapsible form-wrapper collapse-processed"))[0]).css("border","");	    
		     var x = ((document.getElementsByName("formatb"))[0]).value;
		       if(x==0)
                       {

                              $(document.getElementById("edit-bold1")).css("background-color","#0f273a");			       
			      $((document.getElementsByClassName("labelEmail"))).css("font-weight","normal");				   
			      $((document.getElementsByClassName("labelFirstName"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelLastName"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelStreet"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelCity"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelState"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelZipCode"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelCountry"))).css("font-weight","normal");
			   	 	    
		       }	   
                       else
                        {
                              $(document.getElementById("edit-bold1")).css("background-color","#1d4b70");			      				
			      $((document.getElementsByClassName("labelEmail"))).css("font-weight","bold");				   
			      $((document.getElementsByClassName("labelFirstName"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelLastName"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelStreet"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelCity"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelState"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelZipCode"))).css("font-weight","bold");
			      $((document.getElementsByClassName("labelCountry"))).css("font-weight","bold");	 
			}
                     	$(document.getElementById("edit-bold1")).once().click(function() {
				
				var weight = ((document.getElementsByName("formatb"))[0]).value;	
			       if(weight == 0)
			{
                              $(document.getElementById("edit-bold1")).css("background-color","#1d4b70");	
                              $((document.getElementsByClassName("labelEmail"))).css("font-weight","bold");				   
 			      $((document.getElementsByClassName("labelFirstName"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelLastName"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelStreet"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelCity"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelState"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelZipCode"))).css("font-weight","bold");	
			      $((document.getElementsByClassName("labelCountry"))).css("font-weight","bold");	
			      ((document.getElementsByName("formatb"))[0]).value="1";
                            
			}
			       else
			{
                              $(document.getElementById("edit-bold1")).css("background-color","#0f273a");			      				
			      $((document.getElementsByClassName("labelEmail"))).css("font-weight","normal");				   
			      $((document.getElementsByClassName("labelFirstName"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelLastName"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelStreet"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelCity"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelState"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelZipCode"))).css("font-weight","normal");
			      $((document.getElementsByClassName("labelCountry"))).css("font-weight","normal");		                                 				              ((document.getElementsByName("formatb"))[0]).value="0"; 
			}
			});
		 var y = ((document.getElementsByName("formati"))[0]).value;
                        if(y==0)
			{
 		                    $(document.getElementById("edit-1italic1")).css("background-color","#0f273a");			   	
				    $((document.getElementsByClassName("labelEmail"))).css("font-style","normal");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("font-style","normal");	
				    $((document.getElementsByClassName("labelLastName"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelStreet"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelCity"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelState"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelZipCode"))).css("font-style","normal");
			}
			else
			{
              	                    $(document.getElementById("edit-italic1")).css("background-color","#1d4b70");			                                
		                    $((document.getElementsByClassName("labelEmail"))).css("font-style","italic");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("font-style","italic");
				    $((document.getElementsByClassName("labelLastName"))).css("font-style","italic");	
				    $((document.getElementsByClassName("labelStreet"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelCity"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelState"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelZipCode"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelCountry"))).css("font-style","italic");			 				    
			}
		        $(document.getElementById("edit-italic1")).once().click(function() {
				var style = ((document.getElementsByName("formati"))[0]).value;
				if(style==1)
			{
		                    $(document.getElementById("edit-italic1")).css("background-color","#0f273a");			   				
				    $((document.getElementsByClassName("labelEmail"))).css("font-style","normal");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("font-style","normal");	
				    $((document.getElementsByClassName("labelLastName"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelStreet"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelCity"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelState"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelZipCode"))).css("font-style","normal");
				    $((document.getElementsByClassName("labelCountry"))).css("font-style","normal"); 
				    ((document.getElementsByName("formati"))[0]).value="0";
				    
		        }
			        else
			{
		                    $(document.getElementById("edit-italic1")).css("background-color","#1d4b70");			                                
	                            $((document.getElementsByClassName("labelEmail"))).css("font-style","italic");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("font-style","italic");
				    $((document.getElementsByClassName("labelLastName"))).css("font-style","italic");	
				    $((document.getElementsByClassName("labelStreet"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelCity"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelState"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelZipCode"))).css("font-style","italic");			    
				    $((document.getElementsByClassName("labelCountry"))).css("font-style","italic");			    
				    ((document.getElementsByName("formati"))[0]).value="1";
			}

			});
			var z = ((document.getElementsByName("formatu"))[0]).value;
			if(z==0)
			{
         		            $(document.getElementById("edit-under1")).css("background-color","#0f273a");
				    $((document.getElementsByClassName("labelEmail"))).css("text-decoration","none");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("text-decoration","none");		
				    $((document.getElementsByClassName("labelLastName"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelStreet"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelCity"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelState"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelZipCode"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelCountry"))).css("text-decoration","none");	 
			}
			else
			{
		                     $(document.getElementById("edit-under1")).css("background-color","#1d4b70");                                       
				     $((document.getElementsByClassName("labelEmail"))).css("text-decoration","underline");				   
				     $((document.getElementsByClassName("labelFirstName"))).css("text-decoration","underline");	
				     $((document.getElementsByClassName("labelLastName"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelStreet"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelCity"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelState"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelCountry"))).css("text-decoration","underline");
			}
                	$(document.getElementById("edit-under1")).once().click(function() {
				var style = ((document.getElementsByName("formatu"))[0]).value;
				if(style==1)
			{
		                    $(document.getElementById("edit-under1")).css("background-color","#0f273a");					
				    $((document.getElementsByClassName("labelEmail"))).css("text-decoration","none");				   
				    $((document.getElementsByClassName("labelFirstName"))).css("text-decoration","none");		
				    $((document.getElementsByClassName("labelLastName"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelStreet"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelCity"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelState"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelZipCode"))).css("text-decoration","none");	
				    $((document.getElementsByClassName("labelCountry"))).css("text-decoration","none");	
			            ((document.getElementsByName("formatu"))[0]).value = "0";
				    
		        }
			        else
			{
		                     $(document.getElementById("edit-under1")).css("background-color","#1d4b70"); 
                                     $((document.getElementsByClassName("labelEmail"))).css("text-decoration","underline");				   
				     $((document.getElementsByClassName("labelFirstName"))).css("text-decoration","underline");	
				     $((document.getElementsByClassName("labelLastName"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelStreet"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelCity"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelState"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-decoration","underline");
				     $((document.getElementsByClassName("labelCountry"))).css("text-decoration","underline");
				     ((document.getElementsByName("formatu"))[0]).value = "1";
				   
			}
			});
		     var x = ((document.getElementsByName("formatbh"))[0]).value;
		       if(x==0) {
			  $((document.getElementsByClassName("head"))).css("font-weight","normal");   
	                  $(document.getElementById("edit-hbold1")).css("background-color","#0f273a");
		       }	   
                       else {
                             $((document.getElementsByClassName("head"))).css("font-weight","bold"); 
		             $(document.getElementById("edit-hbold1")).css("background-color","#1d4b70");
			
			}
                     	$(document.getElementById("edit-hbold1")).once().click(function() {
				var weight = $((document.getElementById("head"))).css("font-weight");

			  if(weight == "normal"||weight == 200) {
			         $((document.getElementsByClassName("head"))).css("font-weight","bold");  	
		             $(document.getElementById("edit-hbold1")).css("background-color","#1d4b70");
				((document.getElementsByName("formatbh"))[0]).value="1";
                            
			  }
			 else {
                             $((document.getElementsByClassName("head"))).css("font-weight","normal");   
		             $(document.getElementById("edit-hbold1")).css("background-color","#0f273a");		      	     
                              ((document.getElementsByName("formatbh"))[0]).value="0";
			 
			 }
			});
		 var y = ((document.getElementsByName("formatih"))[0]).value;
                        if(y==0)
			{
                                 $((document.getElementsByClassName("head"))).css("font-style","normal");  
		             $(document.getElementById("edit-hitalic1")).css("background-color","#0f273a");			 	 
			    
			}
			else
			{
                                 $((document.getElementsByClassName("head"))).css("font-style","italic");      
		             $(document.getElementById("edit-hitalic1")).css("background-color","#1d4b70");			 	 			     	 
                               
			}
		        $(document.getElementById("edit-hitalic1")).once().click(function() {
				var style = $((document.getElementById("head"))).css("font-style");
				if(style=="italic")
			{
                                 $((document.getElementsByClassName("head"))).css("font-style","normal");        
		             $(document.getElementById("edit-hitalic1")).css("background-color","#0f273a");			 	 			     
				   ((document.getElementsByName("formatih"))[0]).value="0";
				    
		        }
			        else
			{
				 $((document.getElementsByClassName("head"))).css("font-style","italic");    
		             $(document.getElementById("edit-hitalic1")).css("background-color","#1d4b70");			 	 			    			              ((document.getElementsByName("formatih"))[0]).value="1";
				   
			}

			});
			var z = ((document.getElementsByName("formatuh"))[0]).value;
			if(z==0)
			{
                             $((document.getElementsByClassName("head"))).css("text-decoration","none");      
		             $(document.getElementById("edit-hunder1")).css("background-color","#0f273a");			 	 			     	
			}
			else
			{
                             $((document.getElementsByClassName("head"))).css("text-decoration","underline");   
		             $(document.getElementById("edit-hunder1")).css("background-color","#1d4b70");			 	 			     	 			  	 
                                
			}
                	$(document.getElementById("edit-hunder1")).once().click(function() {
				var style = $((document.getElementById("head"))).css("text-decoration");
				if(style=="underline")
			{
                                 $((document.getElementsByClassName("head"))).css("text-decoration","none"); 
		                 $(document.getElementById("edit-hunder1")).css("background-color","#0f273a");
				 ((document.getElementsByName("formatuh"))[0]).value = "0";
				    
		        }
			        else
			{

                                 $((document.getElementsByClassName("head"))).css("text-decoration","underline");   
		                 $(document.getElementById("edit-hunder1")).css("background-color","#1d4b70");
				 ((document.getElementsByName("formatuh"))[0]).value = "1";
				   
			}

			});
                       var x = ((document.getElementsByName("formatl"))[0]).value;
		       if(x==1)
			{
                                 $((document.getElementsByClassName("head"))).css("text-align","left");     
			         $(document.getElementById("edit-left")).css("background-color","#1d4b70");
			         $(document.getElementById("edit-center")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right")).css("background-color","#0f273a");	 				 
			  
			}
                	$(document.getElementById("edit-left")).once().click(function() {
                                 $((document.getElementsByClassName("head"))).css("text-align","left");   
				 ((document.getElementsByName("formatl"))[0]).value = "1";
				 ((document.getElementsByName("formatc"))[0]).value = "0";
				 ((document.getElementsByName("formatr"))[0]).value = "0";
				 $(document.getElementById("edit-left")).css("background-color","#1d4b70");
			         $(document.getElementById("edit-center")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right")).css("background-color","#0f273a");	
			  	 
			
			});
		       var y = ((document.getElementsByName("formatc"))[0]).value;
		       if(y==1)
			{
                                 $((document.getElementsByClassName("head"))).css("text-align","center");          
                                 $(document.getElementById("edit-left")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-right")).css("background-color","#0f273a");					 
			}
                	$(document.getElementById("edit-center")).once().click(function() {
                                 $((document.getElementsByClassName("head"))).css("text-align","center");   
				 ((document.getElementsByName("formatl"))[0]).value = "0";
				 ((document.getElementsByName("formatc"))[0]).value = "1";
				 ((document.getElementsByName("formatr"))[0]).value = "0";
				 $(document.getElementById("edit-left")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-right")).css("background-color","#0f273a");	
			  	 
			
			});
                       var z = ((document.getElementsByName("formatr"))[0]).value;
		       if(z==1)
			{
                                 $((document.getElementsByClassName("head"))).css("text-align","right");  
			         $(document.getElementById("edit-left")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right")).css("background-color","#1d4b70");		 
			  
			}
                	$(document.getElementById("edit-right")).once().click(function() {
                                   $((document.getElementsByClassName("head"))).css("text-align","right");   
				   ((document.getElementsByName("formatl"))[0]).value = "0";
				   ((document.getElementsByName("formatc"))[0]).value = "0";
				   ((document.getElementsByName("formatr"))[0]).value = "1";
				   $(document.getElementById("edit-left")).css("background-color","#0f273a");
			           $(document.getElementById("edit-center")).css("background-color","#0f273a");	 
			           $(document.getElementById("edit-right")).css("background-color","#1d4b70");				  	 
			});
                       var x = ((document.getElementsByName("formatla"))[0]).value;
		       if(x==1)
			{
				     $(document.getElementById("edit-left1")).css("background-color","#1d4b70");
			             $(document.getElementById("edit-center1")).css("background-color","#0f273a");	 
			             $(document.getElementById("edit-right1")).css("background-color","#0f273a");
                                     $((document.getElementsByClassName("Email"))).css("text-align","left");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","left");
				     $((document.getElementsByClassName("LastName"))).css("text-align","left");
				     $((document.getElementsByClassName("Street"))).css("text-align","left");
				     $((document.getElementsByClassName("City"))).css("text-align","left");
				     $((document.getElementsByClassName("State"))).css("text-align","left");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","left");
				     $((document.getElementsByClassName("Country"))).css("text-align","left");
				     
				     $((document.getElementsByClassName("labelEmail"))).css("text-align","left");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","left");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","left");
				     $((document.getElementsByClassName("labelStreet"))).css("text-align","left");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","left");
				     $((document.getElementsByClassName("labelState"))).css("text-align","left");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","left");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","left");
			             $(document.getElementsByClassName("description")).css("text-align","left");				     
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","left");
			  
			}
                	             $(document.getElementById("edit-left1")).once().click(function() {
                                     $((document.getElementsByClassName("Email"))).css("text-align","left");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","left");
				     $((document.getElementsByClassName("LastName"))).css("text-align","left");
				     $((document.getElementsByClassName("Street"))).css("text-align","left");
				     $((document.getElementsByClassName("City"))).css("text-align","left");
				     $((document.getElementsByClassName("State"))).css("text-align","left");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","left");
				     $((document.getElementsByClassName("Country"))).css("text-align","left");
				     
				      $((document.getElementsByClassName("labelEmail"))).css("text-align","left");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","left");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","left");
				      $((document.getElementsByClassName("labelStreet"))).css("text-align","left");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","left");
				     $((document.getElementsByClassName("labelState"))).css("text-align","left");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","left");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","left");
			             $(document.getElementsByClassName("description")).css("text-align","left");
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","left");
				    ((document.getElementsByName("formatla"))[0]).value = "1";
				    ((document.getElementsByName("formatca"))[0]).value = "0";
				    ((document.getElementsByName("formatra"))[0]).value = "0";
				     $(document.getElementById("edit-left1")).css("background-color","#1d4b70");
			         $(document.getElementById("edit-center1")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right1")).css("background-color","#0f273a");
			  	 
			
			});
		       var y = ((document.getElementsByName("formatca"))[0]).value;
		       if(y==1)
			{
				 $(document.getElementById("edit-left1")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center1")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-right1")).css("background-color","#0f273a");
                                     $((document.getElementsByClassName("Email"))).css("text-align","center");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","center");
				     $((document.getElementsByClassName("LastName"))).css("text-align","center");
				     $((document.getElementsByClassName("Street"))).css("text-align","center");
				     $((document.getElementsByClassName("City"))).css("text-align","center");
				     $((document.getElementsByClassName("State"))).css("text-align","center");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","center");
				     $((document.getElementsByClassName("Country"))).css("text-align","center");
				     
				      $((document.getElementsByClassName("labelEmail"))).css("text-align","center");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","center");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","center");
				     $((document.getElementsByClassName("labelStreet"))).css("text-align","center");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","center");
				     $((document.getElementsByClassName("labelState"))).css("text-align","center");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","center");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","center");
			             $(document.getElementsByClassName("description")).css("text-align","center");
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","center");
			  
			}
                	$(document.getElementById("edit-center1")).once().click(function() {
                                      $((document.getElementsByClassName("Email"))).css("text-align","center");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","center");
				     $((document.getElementsByClassName("LastName"))).css("text-align","center");
				     $((document.getElementsByClassName("Street"))).css("text-align","center");
				     $((document.getElementsByClassName("City"))).css("text-align","center");
				     $((document.getElementsByClassName("State"))).css("text-align","center");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","center");
				     $((document.getElementsByClassName("Country"))).css("text-align","center");
				     
				      $((document.getElementsByClassName("labelEmail"))).css("text-align","center");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","center");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","center");
				     $((document.getElementsByClassName("labelStreet"))).css("text-align","center");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","center");
				     $((document.getElementsByClassName("labelState"))).css("text-align","center");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","center");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","center");
			             $(document.getElementsByClassName("description")).css("text-align","center");
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","center");
				   ((document.getElementsByName("formatla"))[0]).value = "0";
				   ((document.getElementsByName("formatca"))[0]).value = "1";
				   ((document.getElementsByName("formatra"))[0]).value = "0";
				    $(document.getElementById("edit-left1")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center1")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-right1")).css("background-color","#0f273a");
			  	 
			
			});
                       var z = ((document.getElementsByName("formatra"))[0]).value;
		       if(z==1)
			{
				 $(document.getElementById("edit-left1")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center1")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right1")).css("background-color","#1d4b70");
                                      $((document.getElementsByClassName("Email"))).css("text-align","right");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","right");
				     $((document.getElementsByClassName("LastName"))).css("text-align","right");
				     $((document.getElementsByClassName("Street"))).css("text-align","right");
				     $((document.getElementsByClassName("City"))).css("text-align","right");
				     $((document.getElementsByClassName("State"))).css("text-align","right");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","right");
				     $((document.getElementsByClassName("Country"))).css("text-align","right");
				     
				      $((document.getElementsByClassName("labelEmail"))).css("text-align","right");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","right");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","right");
				     $((document.getElementsByClassName("labelStreet"))).css("text-align","right");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","right");
				     $((document.getElementsByClassName("labelState"))).css("text-align","right");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","right");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","right");
			             $(document.getElementsByClassName("description")).css("text-align","right");
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","right");
			  
			}
                	$(document.getElementById("edit-right1")).once().click(function() {
				 $(document.getElementById("edit-left1")).css("background-color","#0f273a");
			         $(document.getElementById("edit-center1")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-right1")).css("background-color","#1d4b70");
                                                                            $((document.getElementsByClassName("Email"))).css("text-align","right");
				     $((document.getElementsByClassName("FirstName"))).css("text-align","right");
				     $((document.getElementsByClassName("LastName"))).css("text-align","right");
				     $((document.getElementsByClassName("Street"))).css("text-align","right");
				     $((document.getElementsByClassName("City"))).css("text-align","right");
				     $((document.getElementsByClassName("State"))).css("text-align","right");
				     $((document.getElementsByClassName("ZipCode"))).css("text-align","right");
				     $((document.getElementsByClassName("Country"))).css("text-align","right");
				     
				      $((document.getElementsByClassName("labelEmail"))).css("text-align","right");
				     $((document.getElementsByClassName("labelFirstName"))).css("text-align","right");
				     $((document.getElementsByClassName("labelLastName"))).css("text-align","right");
				     $((document.getElementsByClassName("labelStreet"))).css("text-align","right");
				     $((document.getElementsByClassName("labelCity"))).css("text-align","right");
				     $((document.getElementsByClassName("labelState"))).css("text-align","right");
				     $((document.getElementsByClassName("labelZipCode"))).css("text-align","right");
				     $((document.getElementsByClassName("labelCountry"))).css("text-align","right");
			             $(document.getElementsByClassName("description")).css("text-align","right");
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align","right");
				   ((document.getElementsByName("formatla"))[0]).value = "0";
				   ((document.getElementsByName("formatca"))[0]).value = "0";
				   ((document.getElementsByName("formatra"))[0]).value = "1";
			  	 
			
			});
				/*$(document.getElementById("edit-halign")).once().change(function() {
				     var a = $('#edit-halign').val();
				     var b = $((document.getElementById("head"))).css("text-align");
				     $((document.getElementById("head"))).css("text-align",a);
				     $((document.getElementsByClassName("head"))).css("text-align",a);          
				     
				});*/
	                        	$(document.getElementById("edit-balign")).change(function() {
				     var a = $('#edit-balign').val();
				     $((document.getElementsByClassName("sub1"))[0]).css("text-align",a);
				     $((document.getElementById("button"))).css("text-align",a);
                                     
				});			
		   
		var c = (document.getElementById("edit-opt").value);
		if(c=="Inline") {
				     $((document.getElementsByClassName("labelEmail"))).css("display","block");
				     $((document.getElementsByClassName("labelEmail"))).css("float","left");
				     $((document.getElementsByClassName("labelEmail"))).css("margin","5px");
				     $((document.getElementsByClassName("labelEmail"))).css("width","100px");
                                     $((document.getElementsByClassName("description"))).css("float","right");	                             	
			             $((document.getElementsByClassName("labelFirstName"))).css("display","block");
				     $((document.getElementsByClassName("labelFirstName"))).css("float","left");
				     $((document.getElementsByClassName("labelFirstName"))).css("margin","5px");
				     $((document.getElementsByClassName("labelFirstName"))).css("width","100px");	 
                                     $((document.getElementsByClassName("labelLastName"))).css("display","block");
				     $((document.getElementsByClassName("labelLastName"))).css("float","left");
				     $((document.getElementsByClassName("labelLastName"))).css("margin","5px");
				     $((document.getElementsByClassName("labelLastName"))).css("width","100px");	
				     $((document.getElementsByClassName("labelStreet"))).css("display","block");
				     $((document.getElementsByClassName("labelStreet"))).css("float","left");
				     $((document.getElementsByClassName("labelStreet"))).css("margin","5px");
				     $((document.getElementsByClassName("labelStreet"))).css("width","100px"); 
				     $((document.getElementsByClassName("labelCity"))).css("display","block");
				     $((document.getElementsByClassName("labelCity"))).css("float","left");
				     $((document.getElementsByClassName("labelCity"))).css("margin","5px");
				     $((document.getElementsByClassName("labelCity"))).css("width","100px");
				     $((document.getElementsByClassName("labelState"))).css("display","block");
				     $((document.getElementsByClassName("labelState"))).css("float","left");
				     $((document.getElementsByClassName("labelState"))).css("margin","5px");
				     $((document.getElementsByClassName("labelState"))).css("width","100px");
				     $((document.getElementsByClassName("labelZipCode"))).css("display","block");
				     $((document.getElementsByClassName("labelZipCode"))).css("float","left");
				     $((document.getElementsByClassName("labelZipCode"))).css("margin","5px");
				     $((document.getElementsByClassName("labelZipCode"))).css("width","100px");
				     $((document.getElementsByClassName("labelCountry"))).css("display","block");
				     $((document.getElementsByClassName("labelCountry"))).css("float","left");
				     $((document.getElementsByClassName("labelCountry"))).css("margin","5px");
				     $((document.getElementsByClassName("labelCountry"))).css("width","100px");
                                     $((document.getElementsByClassName("description"))).css("word-wrap","break-word");				     
           			     
				     
		
		}
		else
		{
			             $((document.getElementsByClassName("labelEmail"))).css("display","");
				     $((document.getElementsByClassName("labelEmail"))).css("float","");
				     $((document.getElementsByClassName("labelEmail"))).css("margin","");
				     $((document.getElementsByClassName("labelEmail"))).css("width","");
                                     $((document.getElementsByClassName("description"))).css("float","");				     
			             $((document.getElementsByClassName("labelFisrtName"))).css("display","");
				     $((document.getElementsByClassName("labelFirstName"))).css("float","");
				     $((document.getElementsByClassName("labelFirstName"))).css("margin","");
				     $((document.getElementsByClassName("labelFirstName"))).css("width","");	    
                                     $((document.getElementsByClassName("labelLastName"))).css("display","");
				     $((document.getElementsByClassName("labelLastName"))).css("float","");
				     $((document.getElementsByClassName("labelLastName"))).css("margin","");
				     $((document.getElementsByClassName("labelLastName"))).css("width","");	
				     $((document.getElementsByClassName("labelStreet"))).css("display","");
				     $((document.getElementsByClassName("labelStreet"))).css("float","");
				     $((document.getElementsByClassName("labelStreet"))).css("margin","");
				     $((document.getElementsByClassName("labelStreet"))).css("width",""); 
				     $((document.getElementsByClassName("labelCity"))).css("display","");
				     $((document.getElementsByClassName("labelCity"))).css("float","");
				     $((document.getElementsByClassName("labelCity"))).css("margin","");
				     $((document.getElementsByClassName("labelCity"))).css("width","");
				     $((document.getElementsByClassName("labelState"))).css("display","");
				     $((document.getElementsByClassName("labelState"))).css("float","");
				     $((document.getElementsByClassName("labelState"))).css("margin","");
				     $((document.getElementsByClassName("labelState"))).css("width","");
				     $((document.getElementsByClassName("labelZipCode"))).css("display","");
				     $((document.getElementsByClassName("labelZipCode"))).css("float","");
				     $((document.getElementsByClassName("labelZipCode"))).css("margin","");
				     $((document.getElementsByClassName("labelZipCode"))).css("width","");
				     $((document.getElementsByClassName("labelCountry"))).css("display","");
				     $((document.getElementsByClassName("labelCountry"))).css("float","");
				     $((document.getElementsByClassName("labelCountry"))).css("margin","");
				     $((document.getElementsByClassName("labelCountry"))).css("width","");    
 			     
                   
		}
                 
		 $(document.getElementById("edit-bstyle")).change(function() {
	          var a = (document.getElementById("edit-bstyle")).value;
                 (document.getElementById("font_family2")).innerHTML=a;			    		  
		 $(document.getElementsByName("op1")).css("font-family",a,'important');
 	         $(document.getElementsByClassName("sub1")).css("font-family",a);		 
		 });
                 $(document.getElementById("edit-bsize")).change(function() {
	          var a = (document.getElementById("edit-bsize")).value;
                  (document.getElementById("font_size2")).innerHTML=a;
                  a+="px";		  
		 $(document.getElementsByName("op1")).css("font-size",a,'important');
 	         $(document.getElementsByClassName("sub1")).css("font-size",a);		 
		 });
                     $(document.getElementById("edit-size")).change(function() {
			     
			     var a = (document.getElementById("edit-size")).value;
 		             var align = $(document.getElementById("head")).css("text-align");
			     $(document.getElementById("head")).css("font-size",a);
			     $(document.getElementById("head")).css("text-align",align);
	          });

		      $(document.getElementById("edit-size1")).change(function() {
			     var a = (document.getElementById("edit-size1")).value;
                           (document.getElementById("font_size1")).innerHTML=a;			     
			     a+="px"; 
			     //$(document.getElementsByClassName("Email")).css("font-size",a);
			     //$(document.getElementsByClassName("Name")).css("font-size",a);
			     //$(document.getElementsByClassName("PhoneNo")).css("font-size",a);
			      $(document.getElementsByClassName("labelEmail")).css("font-size",a);
			     $(document.getElementsByClassName("labelFirstName")).css("font-size",a);
			     $(document.getElementsByClassName("labelLastName")).css("font-size",a);
			     $(document.getElementsByClassName("labelStreet")).css("font-size",a);
			     $(document.getElementsByClassName("labelCity")).css("font-size",a);
			     $(document.getElementsByClassName("labelState")).css("font-size",a);
			     $(document.getElementsByClassName("labelZipCode")).css("font-size",a);
			     $(document.getElementsByClassName("labelCountry")).css("font-size",a);
			     
			     //$(document.getElementsByClassName("description")).css("font-size",a);			     
			     
		     });
		        $(document.getElementById("edit-style1")).change(function() {
			     var a = (document.getElementById("edit-style1")).value;
                           (document.getElementById("font_family1")).innerHTML=a;			     
			     //$(document.getElementsByClassName("Email")).css("font-family",a);
			     //$(document.getElementsByClassName("Name")).css("font-family",a);
			     // $(document.getElementsByClassName("PhoneNo")).css("font-family",a);
			      $(document.getElementsByClassName("labelEmail")).css("font-family",a);
			     $(document.getElementsByClassName("labelFirstName")).css("font-family",a);
			     $(document.getElementsByClassName("labelLastName")).css("font-family",a);
			     $(document.getElementsByClassName("labelStreet")).css("font-family",a);
			     $(document.getElementsByClassName("labelCity")).css("font-family",a);
			     $(document.getElementsByClassName("labelState")).css("font-family",a);
			     $(document.getElementsByClassName("labelZipCode")).css("font-family",a);
			     $(document.getElementsByClassName("labelCountry")).css("font-family",a);
			      //$(document.getElementsByClassName("description")).css("font-family",a);
			      
		     });
                       var x = ((document.getElementsByName("formatlb"))[0]).value;
		       if(x==1)
			{
				     $((document.getElementsByClassName("sub1"))).css("text-align","left");
				     $(document.getElementById("edit-bleft")).css("background-color","#1d4b70");
			         $(document.getElementById("edit-bcenter")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-bright")).css("background-color","#0f273a");

			  
			}
                	$(document.getElementById("edit-bleft")).once().click(function() {
				     $((document.getElementsByClassName("sub1"))).css("text-align","left");
				   ((document.getElementsByName("formatlb"))[0]).value = "1";
				   ((document.getElementsByName("formatcb"))[0]).value = "0";
				   ((document.getElementsByName("formatrb"))[0]).value = "0";
				    $(document.getElementById("edit-bleft")).css("background-color","#1d4b70");
			         $(document.getElementById("edit-bcenter")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-bright")).css("background-color","#0f273a");
			  	 
			
			});
		       var y = ((document.getElementsByName("formatcb"))[0]).value;
		       if(y==1)
			{
				     $((document.getElementsByClassName("sub1"))).css("text-align","center");
				     $(document.getElementById("edit-bleft")).css("background-color","#0f273a");
			         $(document.getElementById("edit-bcenter")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-bright")).css("background-color","#0f273a");

			  
			}
                	$(document.getElementById("edit-bcenter")).once().click(function() {
				     $((document.getElementsByClassName("sub1"))).css("text-align","center");
				   ((document.getElementsByName("formatlb"))[0]).value = "0";
				   ((document.getElementsByName("formatcb"))[0]).value = "1";
				   ((document.getElementsByName("formatrb"))[0]).value = "0";
				     $(document.getElementById("edit-bleft")).css("background-color","#0f273a");
			         $(document.getElementById("edit-bcenter")).css("background-color","#1d4b70");	 
			         $(document.getElementById("edit-bright")).css("background-color","#0f273a");

			  	 
			
			});
                       var z = ((document.getElementsByName("formatrb"))[0]).value;
		       if(z==1)
			{
				     $((document.getElementsByClassName("sub1"))).css("text-align","right");
				       $(document.getElementById("edit-bleft")).css("background-color","#0f273a");
			         $(document.getElementById("edit-bcenter")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-bright")).css("background-color","#1d4b70");


			  
			}
                	$(document.getElementById("edit-bright")).once().click(function() {
				     $((document.getElementsByClassName("sub1"))).css("text-align","right");
				   ((document.getElementsByName("formatlb"))[0]).value = "0";
				   ((document.getElementsByName("formatcb"))[0]).value = "0";
				   ((document.getElementsByName("formatrb"))[0]).value = "1";
				     $(document.getElementById("edit-bleft")).css("background-color","#0f273a");
			         $(document.getElementById("edit-bcenter")).css("background-color","#0f273a");	 
			         $(document.getElementById("edit-bright")).css("background-color","#1d4b70");

			  	 
			
			});
			/*$(document.getElementById("edit-align")).change(function() {
				     var a = $('#edit-align').val();
                                     $((document.getElementsByClassName("Email"))).css("text-align",a);
				     $((document.getElementsByClassName("Name"))).css("text-align",a);
				     $((document.getElementsByClassName("PhoneNo"))).css("text-align",a);
				      $((document.getElementsByClassName("labelEmail"))).css("text-align",a);
				     $((document.getElementsByClassName("labelName"))).css("text-align",a);
				     $((document.getElementsByClassName("labelPhoneNo"))).css("text-align",a);
			             $(document.getElementsByClassName("description")).css("text-align",a);
				     $((document.getElementsByClassName("form-item-preview1-Email"))).css("text-align",a);
				     //$((document.getElementById("edit-preview1-email--2"))).css("text-align",a);				     
				     
                                     
				});*/
				/*$(document.getElementById("edit-halign")).once().change(function() {
				     var a = $('#edit-halign').val();
				     var b = $((document.getElementById("head"))).css("text-align");
				     $((document.getElementById("head"))).css("text-align",a);
				     
				});*/
					/*$(document.getElementById("edit-balign")).change(function() {
				     var a = $('#edit-balign').val();
				     $((document.getElementsByClassName("sub1"))).css("text-align",a);
                                     
				});*/
                               
			$(document.getElementById("edit-opt")).change(function() {
                                $("#edit-opt option[value='Label Position']").remove();
			});
                        $(document.getElementById("edit-opt1")).change(function() {
                                $("#edit-opt1 option[value='Description Format']").remove();
			});   
                        $(document.getElementById("edit-des-size")).change(function() {
                                $("#edit-des-size option[value='Size']").remove();
			});  

      var targets = "";
      var first = true;
      // First we initialize some CSS settings - adding the background that the user has chosen etc.
      for (var i = 0; i < Drupal.settings.jqueryColorpicker.ids.length; i++) {
        if (!first) {
          targets += ", ";
        }
        else {
          first = false;
        }
        // This following gives us the ID of the element we will use as a point of reference for the settings
        var id = "#" + Drupal.settings.jqueryColorpicker.ids[i] + "-inner_wrapper";
        // Next we use that point of reference to set a bunch of CSS settings
        $(id).css({"height" : "36px", "width" : "36px", "position" : "relative"})
          .children(".color_picker").css({ "background-repeat" : "no-repeat", "background-position" : "center center", "height" : "25px", "width" : "25px", "position" : "absolute", "top" : "1px", "left" : "3px","background-size" : "25px 25px"})
          .children().css({"display" : "none"});
        // we build a list of IDS that will then be acted upon in the next section of code
       targets += id;
     }
   
     // next we use the list of IDs we just built and act upon each of them
     $(targets).each(function() {
       // First we get a point of reference from which to work
       var target = $(this).attr("id");
       // we set the display of the label to inline. The reason for this is that clicking on a label element
       // automatically sets the focus on the input. With the jquery colorpicker, this means the colorpicker
       // pops up. When the display isn't set to inline, it extends to 100% width, meaning the clickable
       // area is much bigger than it should be, making the 'invisible' clickable space very large.
       // When it's set to inline, the width of the lable is only as wide as the text
       // as big as.
       $("#" + target).parent().siblings("label").css("display",  "inline");
       // next we get the background color of the element
       var defaultColor = $("#" + target + " .color_picker").css("background-color");
      // alert(target + " " +defaultColor);
       // if the background color is an rgb value, which it is when using firefox, we convert it to a
       // hexidecimal number
       if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
      
	     
       }   
          // finally we initialize the colorpicker element. This calls functions provided by the 3rd party code.
         if(target == "edit-element-inner_wrapper")
     {

        var defaultColor = $("#filler3").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
	$((document.getElementById("form"))).css("background","#"+defaultColor); 
	$((document.getElementById("form1"))).css("background","#"+defaultColor); 
	$((document.getElementById("form2"))).css("background","#"+defaultColor); 
	$((document.getElementById("form3"))).css("background","#"+defaultColor); 	
        $((document.getElementById("filler3"))).css("background-color","#"+defaultColor);	 
	
         var trigger = $(this).children(".color_picker");
         trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
                //$("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change(); 
		$((document.getElementById("form"))).css("background","#"+hex); 
         	$((document.getElementById("form1"))).css("background","#"+hex); 
        	$((document.getElementById("form2"))).css("background","#"+hex); 	
        	$((document.getElementById("form3"))).css("background","#"+hex); 			
		$((document.getElementById("head"))).css("background","#"+hex);
              $((document.getElementById("filler3"))).css("background-color","#"+hex);
                ((document.getElementsByName("form_color"))[0]).value = "#"+hex;	      
		
           }
         });
	  
     }
      if(target == "edit-element1-inner_wrapper")
     {
	 var defaultColor = $("#filler4").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
	 $((document.getElementsByName("preview1[Email]"))).css("background","#"+defaultColor);
       	 $((document.getElementsByName("preview1[Name]"))).css("background","#"+defaultColor);
	 $((document.getElementsByName("preview1[Phone]"))).css("background","#"+defaultColor);
	 $((document.getElementsByClassName("formbox"))).css("background","#"+defaultColor); 
              $((document.getElementById("filler4"))).css("background-color","#"+defaultColor);	      
	 
         var trigger = $(this).children(".color_picker");
           trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
              //$("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change();
	       $((document.getElementsByName("preview1[Email]"))).css("background","#"+hex);
       	       $((document.getElementsByName("preview1[Name]"))).css("background","#"+hex);
	       $((document.getElementsByClassName("formbox"))).css("background","#"+hex);
               $((document.getElementById("filler4"))).css("background-color","#"+hex);	      
  	        ((document.getElementsByName("textbox_color"))[0]).value = "#"+hex;	      
		 
           }
         });
     }
      if(target == "edit-element2-inner_wrapper")
      {
        var defaultColor = $("#filler5").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
	 //$("#preview1").css("color","#"+defaultColor);
	 //$((document.getElementsByClassName("head"))).css("color","#"+defaultColor);         	
 $((document.getElementsByClassName("Email"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("FirstName"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("LastName"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("Street"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("City"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("State"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("ZipCode"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("Country"))).css("color","#"+defaultColor);
	 
	 $((document.getElementsByClassName("labelEmail"))).css("color","#"+defaultColor);
	  $((document.getElementsByClassName("labelFirstName"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelLastName"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelStreet"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelCity"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelState"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelZipCode"))).css("color","#"+defaultColor);
	 $((document.getElementsByClassName("labelCountry"))).css("color","#"+defaultColor);
	 //$((document.getElementsByClassName("description"))).css("color","#"+defaultColor);
              $((document.getElementById("filler5"))).css("background-color","#"+defaultColor);	      
         
         var trigger = $(this).children(".color_picker");
         trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
             // $("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change();
	 //$((document.getElementsByClassName("head"))).css("color","#"+hex);         	
	 $((document.getElementsByClassName("Email"))).css("color","#"+hex);
	 $((document.getElementsByClassName("FirstName"))).css("color","#"+hex);
	 $((document.getElementsByClassName("LastName"))).css("color","#"+hex);
	 $((document.getElementsByClassName("Street"))).css("color","#"+hex);
	 $((document.getElementsByClassName("City"))).css("color","#"+hex);
	 $((document.getElementsByClassName("State"))).css("color","#"+hex);
	 $((document.getElementsByClassName("ZipCode"))).css("color","#"+hex);
	 $((document.getElementsByClassName("Country"))).css("color","#"+hex);
	 
	 $((document.getElementsByClassName("labelEmail"))).css("color","#"+hex);
	  $((document.getElementsByClassName("labelFirstName"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelLastName"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelStreet"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelCity"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelState"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelZipCode"))).css("color","#"+hex);
	 $((document.getElementsByClassName("labelCountry"))).css("color","#"+hex);
              $((document.getElementById("filler5"))).css("background-color","#"+hex);	      	 
	 //$((document.getElementsByClassName("description"))).css("color","#"+hex);
	((document.getElementsByName("text_color"))[0]).value = "#"+hex;	      

           }
         });
     }
       if(target == "edit-element3-inner_wrapper")
     {
	var defaultColor = $("#filler1").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
	 $(document.getElementsByName("op1")).css("background","#"+defaultColor);
	 $(document.getElementsByName("op1")).css("border-color","#"+defaultColor);	 
              $((document.getElementById("filler1"))).css("background-color","#"+defaultColor);	      
		
         var trigger = $(this).children(".color_picker");
         trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
              //$("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change(); 
	       $(document.getElementsByName("op1")).css("background","#"+hex);
	       $(document.getElementsByName("op1")).css("border-color","#"+hex);
              $((document.getElementById("filler1"))).css("background-color","#"+hex);	      
	      ((document.getElementsByName("button_color"))[0]).value = "#"+hex;	      
	       
	       
	      
           }
         });
     }
       if(target == "edit-elementhead-inner_wrapper")
     {
        var defaultColor = $("#filler").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
         $((document.getElementsByClassName("head"))).css("color","#"+defaultColor);         	
              $((document.getElementById("filler"))).css("background-color","#"+defaultColor);	      
	 		
         var trigger = $(this).children(".color_picker");
         trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
              //$("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change(); 
              $((document.getElementsByClassName("head"))).css("color","#"+hex); 
              $((document.getElementById("filler"))).css("background-color","#"+hex);
	((document.getElementsByName("head_color"))[0]).value = "#"+hex;	      
	      
	      
           }
         });
     }
        if(target == "edit-element4-inner_wrapper")
     {
	 var defaultColor = $("#filler2").css("background-color");	
	if(defaultColor.match(/rgb/)) {
         defaultColor = rgb2hex(defaultColor);
	}
	 $(document.getElementsByName("op1")).css("color","#"+defaultColor);
              $((document.getElementById("filler2"))).css("background-color","#"+defaultColor);	      
	 
		
         var trigger = $(this).children(".color_picker");
         trigger.ColorPicker({
           color: defaultColor,
           onShow: function (colpkr) {
             $(colpkr).fadeIn(500);
             return false;
           },
           onHide: function (colpkr) {
             $(colpkr).fadeOut(500);
             return false;
           },
           onChange: function (hsb, hex, rgb) {
              //$("#" + target + " .color_picker").css("backgroundColor", "#" + hex).find("input").val(hex).change(); 
	       $(document.getElementsByName("op1")).css("color","#"+hex);
              $((document.getElementById("filler2"))).css("background-color","#"+hex);	      
	((document.getElementsByName("button_text_color"))[0]).value = "#"+hex;	      
	       
	      
           }
         });
     }

       });

     
     
     }
  };
  // This is the conversion function to convert rgb color values to hexadecimal number values
  function rgb2hex(rgb) {
    var result = new String;
    var number = new Number;
    var numbers = rgb.match(/\d+/g), j, result, number;
    for (j = 0; j < numbers.length; j += 1) {
      number = numbers[j] * 1;
      // convert to hex
      number = number.toString(16);
      // enforce double-digit
      if (number.length < 2) {
        number = "0" + number;
      }
      result += number;
    }
    return result;
  };

})(jQuery);




