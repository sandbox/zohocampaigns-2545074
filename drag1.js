/**
 * @file
 */
(function($, Drupal) {
  /**
   * This script adds jQuery slider functionality to transform_slider form element
   */
  Drupal.behaviors.alter1 = {
    attach: function (context, settings) {

	   $('.tabledrag-toggle-weight').text(Drupal.t(''));
/**
 * Stub function. Allows a custom handler when a row is dropped.
 */
Drupal.tableDrag.prototype.onDrop = function () {
 document.getElementById("edit-weight0").click();  
};
/**
 * Stub function. Allows a custom handler when a row is swapped.
 */
Drupal.tableDrag.prototype.row.prototype.onSwap = function (swappedRow) {
 
  return '';
};

Drupal.theme.prototype.tableDragChangedMarker = function () {
  return '';
};

Drupal.theme.prototype.tableDragIndentation = function () {
  return '';
};

Drupal.theme.prototype.tableDragChangedWarning = function () {
 return '';
};
}
}
})(jQuery, Drupal);

